<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
require "bootstrap.php";
use Chatter\Models\User;


 

$app = new \Slim\App();

//שליפת נתונים
$app->get('/users', function($request, $response,$args){
    $_user = new User();
    $users = $_user->all();
    $payload = [];
    foreach($users as $usr){
         $payload[$usr->id] = [
             'id'=>$usr->id,
             'name'=>$usr->name,
             'phone'=>$usr->phone,
             'created_at'=>$usr->created_at,
             'updated_at'=>$usr->updated_at
         ];
    }
    return $response->withStatus(200)->withJson($payload);
 });


$app->post('/users', function($request, $response,$args){
    $user = $request->getParsedBodyParam('name','');
    $phone = $request->getParsedBodyParam('phone','');
    $_user = new User();
    $_user->name = $user;
    $_user->phone = $phone;
    $_user->save();
       
    if($_user->id){
        $payload = ['id' => $_user->id];
        return $response->withStatus(201)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});

$app->put('/users/{id}', function($request, $response, $args){
    $user = $request->getParsedBodyParam('name','');
    $phone = $request->getParsedBodyParam('phone','');
    $_user = User::find($args['id']);
    $_user->name = $user;
    $_user->phone = $phone;
   
    if($_user->save()){
        $payload = ['id' => $_user->id,"result" => "The user has been updates successfuly"];
        return $response->withStatus(200)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});

$app->delete('/users/{id}', function($request, $response,$args){
    $user = User::find($args['id']);
    $user->delete();
    
    if($user->exists){
        return $response->withStatus(400);
    }
    else{
       return $response->withStatus(200);
   }
});

$app->post('/users/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody();

    User::insert($payload);
    return $response->withStatus(201)->withJson($payload);
});
//get user
$app->get('/users/{id}', function($request, $response,$args){    
    $_user = User::find($args['id']);

    $payload=[];

    if($_user->id){
      return $response->withStatus(200)->withJson(json_decode($_user))->withHeader('Access-Control-Allow-Origin', '*');
    }else{
      return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
    }  
    
});

//הרשאה לאבטחת מידע same origin policy

$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

$app->run();