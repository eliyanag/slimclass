<?php
require 'config/credentials.php';
require 'vendor/autoload.php';

Use \Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule();

$capsule->addConnection([
   'driver' => 'mysql',
   'host' => $db_host,
   'database' => $db_name,
   'username' => $db_user,
   'password' => $db_password,
   'charset'  => 'utf8',
   'collation' => 'utf8_unicode_ci', 
   "prefix" => '',
]);
/*CREATE TABLE `users` (
 `id` int(10) NOT NULL PRIMARY KEY AUTO_INCREMENT,
 `name` varchar(255) NOT NULL,
 `phone-number` varchar(255) NOT NULL,
 `created_at` datetime NOT NULL,
 `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;


*/
$capsule->bootEloquent();